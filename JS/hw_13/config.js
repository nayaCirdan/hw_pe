const themes = {
    blue: {
        '--logoColor': '#4BCAFF',
        '--navColor': '#35444F',
        '--navColorHover': '#222F3A',
        '--footerColor': '#B4B7BA',
        '--sidebarColorHover': '#DBDBDB',
    },
    orange: {
        '--logoColor': '#ff9427',
        '--navColor': '#cc7723',
        '--navColorHover': '#99581c',
        '--footerColor': '#ffc499',
        '--sidebarColorHover': '#ffb580',
    },
};

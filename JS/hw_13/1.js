﻿const btnChangeTheme=document.createElement('button');
btnChangeTheme.innerText="Сменить тему";

localStorage.removeItem('theme');

const btnContainer=document.querySelector('.header-logo');
btnChangeTheme.classList.add('btn-theme');
btnChangeTheme.setAttribute('name','blue');

btnContainer.appendChild(btnChangeTheme);

const style = document.documentElement.style;


function setColors(scheme) {
    if (scheme) {

        style.setProperty('--logoColor', themes[scheme]['--logoColor']);
        style.setProperty('--navColor', themes[scheme]['--navColor']);
        style.setProperty('--navColorHover', themes[scheme]['--navColorHover']);
        style.setProperty('--footerColor', themes[scheme]['--footerColor']);
        style.setProperty('--sidebarColorHover', themes[scheme]['--sidebarColorHover']);
    }
}


const setTheme = (event) => {
    let scheme = event.target.name;
    if (btnChangeTheme.name === 'blue') {
        scheme = 'orange';
        if (themes.hasOwnProperty(scheme)) {
            setColors(scheme);
            btnChangeTheme.name = 'orange';
            localStorage.setItem(`scheme`, scheme);
        }
    } else
        if (btnChangeTheme.name === 'orange') {
            scheme='blue';
            if (themes.hasOwnProperty(scheme)) {
                btnChangeTheme.name = 'blue';
                setColors(scheme);
                localStorage.setItem(`scheme`, scheme);
            }
        }
};

document.addEventListener('DOMContentLoaded', () => {
    const scheme=localStorage.getItem('scheme');
    if (scheme !== null) {
        btnChangeTheme.setAttribute('name', scheme);
    } else {
        btnChangeTheme.setAttribute('name', 'blue');
    }

    setColors(scheme);
});

btnChangeTheme.addEventListener('click',setTheme);
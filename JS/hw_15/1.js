
const nav=$('.nav-bar');

nav.on ('click','a',function (ev) {
    ev.preventDefault();
 let sectionLink=$(this).attr('href');
 let sectionOffset=$(sectionLink).offset().top;
 $(document.body.parentElement).animate({scrollTop: sectionOffset},1000)
     });

const btnSlide=$('.btn-slide-section');

btnSlide.click(()=>{
    $('.conteiner-images').slideToggle();
    btnSlide.toggleClass('active');

    if(btnSlide.hasClass('active')) {
        btnSlide.text('Show');
    } else {
        btnSlide.text('Hide');
    }
});

const buttonUp = $('.btn-scroll-up');
buttonUp.hide();
$(window).scroll(() => {
    const windowHeight = $(window).innerHeight();
    const offsetTop = $(window).scrollTop();

    if (offsetTop > windowHeight) {
        buttonUp.fadeIn();
    } else {
        buttonUp.fadeOut();
    }
});

buttonUp.click(() => {
        $(document.body.parentElement).animate({ scrollTop: 0 }, 800);
    });


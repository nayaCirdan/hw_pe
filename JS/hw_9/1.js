const tab=document.querySelector('.tabs');
let lastTab=document.querySelector('.tabs-title.active');
tab.addEventListener('click', (event)=>{
    lastTab.classList.remove('active');
    lastTab=event.target;
    lastTab.classList.add('active');


    const tabDataSet=event.target.dataset.tab;
    const contentList=document.querySelectorAll('.content-item');


    for (let i=0; i<contentList.length; i++) {
        let contentItem=contentList[i];
        const contentDataSet=contentItem.dataset.content;

        if (tabDataSet===contentDataSet) {

            contentItem.classList.add('active');
            break;
        } else {
            contentItem.classList.remove('active');
        }
    }
});


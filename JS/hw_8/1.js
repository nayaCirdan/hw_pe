/*Некоторые стили добавлены через css, некоторые, через js*/

const inputPrice=document.createElement('input');
inputPrice.setAttribute('type', 'number');
inputPrice.setAttribute('id', 'price');
document.body.prepend(inputPrice);


const labelPrice=document.createElement('label');
labelPrice.setAttribute('for', 'price');
labelPrice.innerText='Price';
document.body.prepend(labelPrice);

inputPrice.addEventListener('focus', ()=>{
    inputPrice.style.borderColor='green';
});

inputPrice.addEventListener('blur', ()=>{


    let priceValue=inputPrice.value;

    if (Number(priceValue) < 0 || priceValue===''){ //Добавила еще проверку на пустую строку, чтобы не вылезал спан с пустой ценой
        inputPrice.style.borderColor='red';
        inputPrice.style.borderWidth='1px';
        let errorMessage=document.createElement('span');
        errorMessage.classList.add('incorrect');
        errorMessage.innerText='Please enter correct price';
        errorMessage.style.color='red';
        errorMessage.style.display='block';
        inputPrice.insertAdjacentElement('afterend', errorMessage);

        //Добавила удаление сообщения о неправильно введенных данных, чтобы они не скапливались
        inputPrice.addEventListener('focus',()=>{
            errorMessage.remove();
        })
    } else {
        const currentPrice = document.createElement('span');
        currentPrice.style.display = 'block';
        currentPrice.classList.add('current-price');
        currentPrice.innerText = `Текущая цена: $${priceValue}`;
        labelPrice.insertAdjacentElement('beforebegin',currentPrice);

        const closeBtn = document.createElement('button');
        closeBtn.innerText = 'X';
        currentPrice.appendChild(closeBtn);

        inputPrice.style.color = 'green';

        closeBtn.addEventListener('click', () => {
            closeBtn.remove();
            currentPrice.remove();
            inputPrice.value = 0; //можно было бы написать '0' как строку, но это не влияет на отображение, а так как input у нас типа number, то лучше 0 - числом
        });

    }
});

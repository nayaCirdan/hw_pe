let content=$('.content-item');
let tab=$('.tabs-title');
content.hide();
$('.content-item:first-child').show();


tab.on('click',function () {
    $('.active').removeClass('active');
    $(this).addClass('active');
    content.hide().eq($(this).index()).fadeIn();
});

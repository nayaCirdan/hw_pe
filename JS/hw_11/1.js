const btnList=document.querySelectorAll('.btn');


document.body.addEventListener('keydown', (event) => {

    btnList.forEach(button => {
        button.style.backgroundColor = '#33333a';
        if (button.innerText.toLowerCase() === event.key || button.innerText === event.key) {

            button.style.backgroundColor = 'blue';
        } else {
            console.log('No such button in document')
        }
    });
});

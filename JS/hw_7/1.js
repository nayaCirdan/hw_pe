//ПЕРВЫЙ ВАРИАНТ С ШАБЛОННОЙ СТРОКОЙ И innerHTML
arr=['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'];
const ulElement = document.createElement('ul');
document.body.appendChild(ulElement);
function createLiElement(arr) {
    arr.map((item) => {
            //const ulElement = document.querySelector('ul');

            let newLiElement=document.createElement('li');
            newLiElement.innerHTML=`<li>${item}</li>`;
            ulElement.append(newLiElement);
        }
    )
}
createLiElement(arr);


//ВТОРОЙ ВАРИАНТ БЕЗ ШАБЛОННОЙ СТРОЙКИ И С innerTEXT
/*arr=['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'];
const ulElement = document.createElement('ul');
document.body.appendChild(ulElement);
function createLiElement(arr) {
    arr.map((text) => {

           let newLiElement=document.createElement('li');
            newLiElement.innerText=text;
            ulElement.append(newLiElement);
        }
    )
}
createLiElement(arr);*/
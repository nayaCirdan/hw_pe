const imagesList= document.querySelectorAll('.image-to-show');
const btnStop=document.createElement('button');
btnStop.innerText="Прекратить";
const btnResume=document.createElement('button');
btnResume.innerText="Возобновить показ";


let i=1;
let firstImg=imagesList[0];

window.onload=()=>{
    imagesList.forEach((img)=>{
        img.classList.add('hidden');
    });
    document.body.appendChild(btnStop);
    document.body.appendChild(btnResume);
    let currImg=firstImg;
    currImg.classList.remove('hidden');
};



imgTimer=setInterval(change,  2*1000);
let prevImg=firstImg;

function change() {
    debugger;
    if (prevImg) {
        prevImg.classList.add('hidden');
    }
    if (i===imagesList.length) {
        i=0;
    }
        currImg=imagesList[i];
        currImg.classList.remove('hidden');
        prevImg=currImg;
        i++;
}

btnStop.addEventListener('click', () => {
    clearInterval(imgTimer);
});

btnResume.addEventListener('click', () => {
    setInterval(change,  2*1000);
});

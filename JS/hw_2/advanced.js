
let m = +prompt('Введите начальное число');
let n = +prompt('Введите конечное число');

while (m>=n) {
    alert('Ошибка, начальное число должно быть меньше, чем конечное');
    m = +prompt('Введите начальное число');
    n = +prompt('Введите конечное число');

    if(m<n) {
        break;
    }
}
mainLoop:
    for (let i=m; i<=n; i++){
        if (i<=1) {
            i=1;
            continue mainLoop;
        }
        for (let j=2; j<i; j++) {
            if (i%j === 0 || i<1) {
                continue mainLoop;
            }
        }
        console.log(i);
    }
let  btnGet=document.querySelector('#get-cash');
let cashResult=document.querySelector('#cash-request-result');
let getCashResult=undefined;

let creditCard={
    money:10000,
    pin: '1111',
    tries:3,
    status: 'active',
    getCash(pinCode,cashAmount) {
        if (this.status === 'disabled') {

            return getCashResult="Ваша карта заблокирована, обратитесь в банк для ее разблокировки";
        }
        if (pinCode!==this.pin) {
            this.tries=this.tries-1;
            if (this.tries>0) {

                return getCashResult="Неправильный пин-код! Попробуйте пожалуйста снова!";
            } else {
                this.status='disabled';

                return getCashResult="Неправильный пин-код! Вы исчерпали количество попыток. Ваша карта заблокирована, обратитесь в банк для ее разблокировки";
            }
        }
        else {
            if(cashAmount<=this.money) {
                this.money=this.money-cashAmount;
                this.tries=3;

                return getCashResult=`Получите ваши ${cashAmount}`;
            } else {
                this.tries=3;

                return getCashResult="К сожалению, на вашем счету недостаточно средств";
            }
        }
    }
};




btnGet.addEventListener('click', ()=>{
    const enteredPin=document.querySelector('#pin-code').value;
    const enteredMoney=document.getElementById('money-sum').value;
    if (!enteredPin || !enteredMoney) {
        getCashResult="Введите значения в поля формы";
    } else {
        creditCard.getCash(enteredPin,enteredMoney);
    }
    cashResult.innerText=`${getCashResult}`;
});
